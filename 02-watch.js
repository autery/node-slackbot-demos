const fetch = require("node-fetch");
const WebSocket = require('ws');

async function get(endpoint) {
    const resp = await fetch(`https://slack.com/api/${endpoint}`, {
        headers: { authorization: process.env.SLACK_AUTH }
    })
    return resp.json()
}

async function setup() {
    resp = await get('rtm.connect')
    new WebSocket(resp.url).on('message', handleMessage)
}

function handleMessage(message) {
    const parsed = JSON.parse(message)
    console.log(parsed)
}

setup()
