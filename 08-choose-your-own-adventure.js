const fs = require('fs')
const app = require('express')()
const bodyParser = require('body-parser')

const game_data = {}

function setup() {
    const file = fs.readFileSync('game.txt', 'utf8')
    game_data.places = file.split('place').slice(1).map(makePlace)
    console.log(JSON.stringify(game_data, null, 2))
}

function makePlace(data) {
    const place = {
        choices: []
    }

    data.trim().match(/[a-z]+:.+?(?=[a-z]+:|$)/gs).map(match => {
        makeSetting(place, match.trim())
    })
    return place
}

function makeSetting(place, setting) {
    const fields = setting.split(': ')
    if(fields[0] != 'choice') {
        place[fields[0]] = fields[1]
    } else {
        const choiceFields = fields[1].split('; ')
        place.choices.push({
            key: choiceFields[0],
            value: choiceFields[1]
        })
    }
}

app.use(bodyParser.urlencoded({type: '*/*'}))

app.post('/go_north', (_, res) => {
    const start = game_data.places[0]
    res.json(getPlace(start))
})

app.post('*', (req, res) => {
    const message = JSON.parse(req.body.payload)
    const choice = message.actions[0].value
    const place = game_data.places.find( place => place.name == choice )
    res.json(getPlace(place))
})

function getPlace(place) {
    const actions = place.choices.map(choice => ({
        name: choice.key,
        text: choice.value,
        type: 'button',
        value: choice.key
    }))

     return {
        text: `*${place.name}*`,
        attachments: [{
            callback_id: 'button',
            color: 'good',
            text: place.desc,
            actions
        }]
    }
}

setup()
app.listen(3000)
