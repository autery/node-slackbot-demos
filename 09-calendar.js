const fs = require('fs')
const ical = require('node-ical')
const fetch = require("node-fetch");

const now = new Date()
const newNotifications = []
const notifiedFile = `${__dirname}/notified.dat`

function post(endpoint, message) {
  fetch(`https://slack.com/api/${endpoint}`, {
    method: 'post',
    headers: {
      authorization: process.env.SLACK_AUTH,
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: JSON.stringify(message)
  })
}

function log(msg) {
  console.log(`${now.toLocaleString()} ${msg}`)
}

function notify(event) {
  console.log(event.id)
  const { summary, start, location } = event
  const text = ["Next meeting:", summary, start.toLocaleString(), location].join('\n')
  const message = { channel: 'CD3R11A49', text }

  post('chat.postMessage', message)
  log(`Sent notification for ${summary}`)

  fs.appendFileSync(notifiedFile, `${event.id}\n`);
}

function firstCurrent(dateList, _default) {
  const filtered = dateList.filter(d => d > now)
    .sort((a,b) => a - b)
  return date = filtered.length ? filtered[0] : _default
}

function setup() {
  const notified = fs.readFileSync(notifiedFile, 'utf8').split(/\n/)

  ical.fromURL(process.env.CALENDAR_ADDRESS, {}, function(err, data) {
    const events = Object.values(data)
      .filter(e => e.type == 'VEVENT')
    events.forEach(e => {
      e.id = `${e.uid}.${+e.start}`
      if (e.recurrences) e.start = firstCurrent(Object.keys(e.recurrences).map(r => new Date(r)), e.start)
    })

    const current = events.filter(e => e.start > now)
      .sort((a, b) => a.start - b.start)
    const first = current[0]

    if ((first.start - now) < (1000 * 60 * 11) && !notified.includes(first.id)) notify(first)
    else log('No notifications')
  })

}

setup()
