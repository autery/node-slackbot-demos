Lunch & Learn 3: Lunch hard with a vengeance!
=============================================

This shows some examples of interacting with the Slack API via node.js, including
basic queries for server info (users, channels), using the real-time messaging API
with websockets, and receving interactive events over Slack-to-server callbacks.

Lastly, we put it all together to create a TODO MVP, and a choose-your-own-adventure
framework.