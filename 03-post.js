const fetch = require("node-fetch");

function post(endpoint, message) {
    fetch(`https://slack.com/api/${endpoint}`, {
        method: 'post',
        headers: {
            authorization: process.env.SLACK_AUTH,
            'Content-Type': 'application/json; charset=utf-8'
        },
        body: JSON.stringify(message)
    })
}

function setup() {
    message = {
        channel: 'DA5920TQQ',
        text: 'Test message!'
    }

    post('chat.postMessage', message)
}
setup()
