const fetch = require("node-fetch");
const WebSocket = require('ws');
const app = require('express')()
const bodyParser = require('body-parser')

const users = {}
const authorization = process.env.SLACK_AUTH

async function get(endpoint) {
    const resp = await fetch(`https://slack.com/api/${endpoint}`, {
        headers: { authorization }
    })
    return resp.json()
}

function post(endpoint, message) {
    fetch(`https://slack.com/api/${endpoint}`, {
        method: 'post',
        headers: { authorization, 'Content-Type': 'application/json; charset=utf-8' },
        body: JSON.stringify(message)
    })
}

async function setup() {
    let resp = await get('users.list')
    resp.members.filter(u => !u.is_bot).forEach(user => {
        users[user.id] = {
            id: user.id,
            name: user.name,
            tasks: []
        }
    })

    resp = await get('rtm.connect')
    new WebSocket(resp.url).on('message', handleMessage)
}

function handleMessage(message) {
    const data = JSON.parse(message)
    if (data.type != 'message') return
    if (!data.user) return

    const user = users[data.user]
    if (!user || user.is_bot) return

   const { text, channel } = data
   if (text == 'tasks') showTasks(user, channel)
}

function tasksFor(user) {
    const text = user.tasks.join('\n')

    return {
        text: `Tasks for ${user.name}`,
        attachments: [{
            callback_id: user.id,
            color: 'good',
            text,
            actions: [
                { name: "finish", text: "Finish tasks", type: "button", value: "finish" },
                { name: "add", text: "Add tasks", type: "button", value: "add" }
            ]
        }]
    }
}

function showTasks(user, channel) {
    message = tasksFor(user)
    message.channel = channel
    post('chat.postMessage', message)
}

app.use(bodyParser.urlencoded({type: '*/*'}))
app.post('*', (req, res) => {
    const message = JSON.parse(req.body.payload)
    const user = users[message.callback_id]
    const action = message.actions[0].value
    if (action == 'add') addTask(user)
    else finishTask(user)
    res.json(tasksFor(user))
})

function addTask(user) {
    user.tasks.push("Task " + (user.tasks.length + 1))
}

function finishTask(user) {
    const index = user.tasks.findIndex(t => !/^~/.test(t))
    if (index == -1) return

    user.tasks[index] = user.tasks[index].replace(/.+/, "~$&~")
}

setup()
app.listen(3000)
