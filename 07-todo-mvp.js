const fetch = require("node-fetch");
const WebSocket = require('ws');
const app = require('express')()
const bodyParser = require('body-parser')

const users = {}
const authorization = process.env.SLACK_AUTH

async function get(endpoint) {
    const resp = await fetch(`https://slack.com/api/${endpoint}`, {
        headers: { authorization }
    })
    return resp.json()
}

async function post(endpoint, message) {
    const resp = await fetch(`https://slack.com/api/${endpoint}`, {
        method: 'post',
        headers: { authorization, 'Content-Type': 'application/json; charset=utf-8' },
        body: JSON.stringify(message)
    })
    return resp.json()
}

async function setup() {
    let resp = await get('users.list')
    resp.members.filter(u => !u.is_bot).forEach(user => {
        users[user.id] = {
            id: user.id,
            name: user.name,
            tasks: []
        }
    })

    resp = await get('rtm.connect')
    new WebSocket(resp.url).on('message', handleMessage)
}

function handleMessage(message) {
    const data = JSON.parse(message)
    if (data.type != 'message') return
    if (!data.user) return

    const user = users[data.user]
    if (!user || user.is_bot) return

   if (data.text == 'tasks') {
       showTasks(user, data.channel)
   }
}

function tasksFor(user, channel) {
    const text = user.tasks.join('\n')

    return {
        channel,
        text: `Tasks for ${user.name}`,
        attachments: [{
            callback_id: 'button',
            color: 'good',
            text,
            actions: [
                { name: "finish", text: "Finish tasks", type: "button", value: "finish" },
                { name: "add", text: "Add tasks", type: "button", value: "add" }
            ]
        }]
    }
}

function showTasks(user, channel) {
    message = tasksFor(user, channel)
    post('chat.postMessage', message).then(r => user.tasks.ts = r.ts )
}

function updateTasks(user, channel) {
    message = tasksFor(user, channel)
    message.ts = user.tasks.ts
    post('chat.update', message)
}

app.use(bodyParser.urlencoded({type: '*/*'}))
app.post('*', (req, res) => {
    const message = JSON.parse(req.body.payload)
    const user = users[message.user.id]
    const callback_id = message.callback_id

    if (callback_id == 'button') {
        process_button(message, user)
        return res.end()
    }

    if (callback_id == 'add') {
        user.tasks.push(message.submission.task)
    } else {
        finishTask(user, message.submission.task)
    }
    res.end()
    updateTasks(user, message.channel.id)
})

function process_button(message, user) {
    const { trigger_id, actions } = message
    const dialog = actions[0].value == 'add' ?
        newTaskName() : finishTaskName(user)

    post('dialog.open', {trigger_id, dialog})
}

function newTaskName() {
    return {
        callback_id: 'add',
        title: 'Add a TODO item',
        submit_label: 'Add',
        elements: [{ type: 'text', label: 'Task name', name: 'task' }]
    }
}

function finishTaskName(user) {
    const options = user.tasks.filter(t => !/^~/.test(t))
        .map(task => ({ label: task, value: task }))

    return {
            callback_id: 'finish',
            title: 'Choose an item to finish',
            submit_label: 'Finish',
            elements: [{ type: 'select', label: 'Task name', name: 'task', options }]
    }
}

function finishTask(user, name) {
    const index = user.tasks.indexOf(name)
    user.tasks[index] = user.tasks[index].replace(/.+/, "~$&~")
}

setup()
app.listen(3000)
