const fetch = require("node-fetch");

async function get(endpoint) {
    const resp = await fetch(`https://slack.com/api/${endpoint}`, {
        headers: { authorization: process.env.SLACK_AUTH }
    })
    return resp.json()
}

async function setup() {
    let resp = await get('users.list')
    console.log('Users:', resp.members.map(u => u.name))

    resp = await get('channels.list')
    console.log('Channels:', resp.channels.map(c => c.name))
}

setup()
