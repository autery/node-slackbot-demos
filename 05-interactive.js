const fetch = require("node-fetch");
const app = require('express')()

function post(endpoint, message) {
    fetch(`https://slack.com/api/${endpoint}`, {
        method: 'post',
        headers: {
            authorization: process.env.SLACK_AUTH,
            'Content-Type': 'application/json; charset=utf-8'
        },
        body: JSON.stringify(message)
    })
}

function setup() {
    message = {
        channel: 'DA5920TQQ',
        attachments: [{
            callback_id: 'click',
            color: 'good',
            text: 'Click the button to replace this message',
            actions: [{ name: "click", text: "Click me", type: "button", value: "click" }]
        }]
    }

    post('chat.postMessage', message)
}
setup()

app.post('*', (_, res) => {
    res.json({ text: "Click received!" })
})
app.listen(3000)
